# base docker image
FROM openjdk:17
EXPOSE 80
ADD  target/cp-1.0.war wcocp.war
ENTRYPOINT ["java","war","wcocp.war"]