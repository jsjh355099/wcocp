package wcocp.cp;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.Scanner;

@SpringBootApplication
public class CpApplicationMain {

	public static void main(String[] args) {
		// IOC容器 (Inversion of Control 控制反轉)
		ConfigurableApplicationContext run = SpringApplication.run(CpApplicationMain.class, args);
//		顯示所有被boot管控的bean
//		String[] names = run.getBeanDefinitionNames();
//		for (String name : names) {
//			System.out.println(name);
//		}
		System.err.println("<-------------------- !!!launch!!! -------------------->");
//		Scanner s = new Scanner(System.in);
//		int sss = s.nextInt();
//		System.out.println(sss);
	}
}