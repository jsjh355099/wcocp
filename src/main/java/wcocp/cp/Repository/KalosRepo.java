package wcocp.cp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import wcocp.cp.Bean.Kalos;

import java.util.List;

@Repository
public interface KalosRepo extends JpaRepository<Kalos, Integer> {

    Boolean existsByAccount(String account);

    @Query(nativeQuery = true,value = "SELECT idkalos FROM wcocp.kalos WHERE  account = :account ;")
    Integer findIdByAccount(@Param("account") String account);

    @Query(nativeQuery = true,value = "SELECT password FROM wcocp.kalos WHERE  idkalos = :idkalos ;")
    String findPasswordById(@Param("idkalos") Integer idkalos);

    Kalos findByAccount(String account);

    List<Kalos> findByAge(Integer age);
    List<Kalos> findByHeight(Integer height);
    List<Kalos> findByWeight(Integer weight);
    @Query(nativeQuery = true,value = "SELECT * FROM wcocp.kalos WHERE  age = :age && height = :height ;")
    List<Kalos> findByAgenHeight(@Param("age") Integer age,@Param("height") Integer height);
    @Query(nativeQuery = true,value = "SELECT * FROM wcocp.kalos WHERE  age = :age && weight = :weight ;")
    List<Kalos> findByAgenWeight(@Param("age") Integer age, @Param("weight") Integer weight);
    @Query(nativeQuery = true,value = "SELECT * FROM wcocp.kalos WHERE  height = :height && weight = :weight ;")
    List<Kalos> findByHeightnWeight(@Param("height") Integer height, @Param("weight") Integer weight);
    @Query(nativeQuery = true,value = "SELECT * FROM wcocp.kalos WHERE  age = :age && height = :height && weight = :weight ;")
    List<Kalos> findByAgenHeightnWeight(@Param("age") Integer age,@Param("height") Integer height, @Param("weight") Integer weight);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "UPDATE wcocp.kalos SET account = :account ,age = :age ,height = :height,weight = :weight WHERE  idkalos = :idkalos ;")
    void updateById(@Param("idkalos") Integer idkalos,@Param("account") String account,  @Param("age") Integer age, @Param("height") Integer height, @Param("weight") Integer weight);

}