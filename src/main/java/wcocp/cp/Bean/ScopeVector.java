package wcocp.cp.Bean;

import lombok.Data;

import java.util.List;

@Data
public class ScopeVector {

	private String scopeName;

	private List<Double> paramCoeff;
}
