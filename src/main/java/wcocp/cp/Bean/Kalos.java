package wcocp.cp.Bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kalos")
public class Kalos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idkalos")
	private Integer idkalos;

	@Column(name = "account")
	private String account;

	@Column(name = "password")
	private String password;

	@Column(name = "age")
	private Integer age;

	@Column(name = "height")
	private Integer height;

	@Column(name = "weight")
	private Integer weight;

//	private Set<ScopeVector> scopeVectorSet;
}
