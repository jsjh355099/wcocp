package wcocp.cp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wcocp.cp.Bean.Kalos;
import wcocp.cp.Repository.KalosRepo;

import java.util.List;
import java.util.Optional;

@RestController
public class Membercontroller {
    @Autowired
    KalosRepo kalosRepo;

    //利用ID唯一性 在資料庫搜尋單個使用者資料
    @GetMapping("/getbyid")
    public Optional<Kalos> getbyid(@RequestParam("idkalos")Integer idkalos){
        return kalosRepo.findById(idkalos);
    }
    //利用帳號唯一性 在資料庫搜尋單個使用者資料
    @GetMapping("/getbyaccount")
    public Kalos getbyaccount(@RequestParam("account")String account){
        return kalosRepo.findByAccount(account);
    }
    //設定查詢條件 找尋資料庫中符合資格的所有使用者
    @GetMapping("/getbyparam")
    public List<Kalos> getBybyparams(@RequestParam(value = "age", required = false)Integer age,@RequestParam(value = "height", required = false)Integer height,@RequestParam(value ="weight", required = false)Integer weight){
        if(age == null){
            if(height == null){
                if(weight == null){
                    return kalosRepo.findAll();
                }else{
                    return kalosRepo.findByWeight(weight);
                }
            }else{
                if(weight == null){
                    return kalosRepo.findByHeight(height);
                }else{
                    return kalosRepo.findByHeightnWeight(height,weight);
                }
            }
        }else{
            if(height == null){
                if(weight == null){
                    return kalosRepo.findByAge(age);
                }else{
                    return kalosRepo.findByAgenWeight(age,weight);
                }
            }else{
                if(weight == null){
                    return kalosRepo.findByAgenHeight(age,height);
                }else{
                    return kalosRepo.findByAgenHeightnWeight(age,height,weight);
                }
            }
        }
    }

    //利用除了idkalos以外的參數 創建新的使用者
    @PostMapping("/post")
    public String post(@RequestBody Kalos kalos){
        if(kalosRepo.existsByAccount(kalos.getAccount())){
            return "Post Failed! Account already existed!";
        }else{
            kalosRepo.save(kalos);
            return  "Post Successed!";
        }
    }
    //使用參數idkalos和新的Entity 更新除了密碼以及idkalos 以外的使用者者資料
    @PutMapping("/put")
    public String put(@RequestParam( value = "idkalos",required = false) Integer idkalos , @RequestBody Kalos kalos){
        if(idkalos == null ){
            return "Need paramID to update!";
        }else if(kalosRepo.existsByAccount(kalos.getAccount())){
            return "Account already existed!";
        } else{
            if(kalosRepo.findPasswordById(idkalos).equals(kalos.getPassword())){
                kalosRepo.updateById(idkalos,kalos.getAccount(),kalos.getAge(),kalos.getHeight(),kalos.getWeight());
                return  "Update Success!";
            }else{
                System.err.println("update error! password: "+ kalos.getPassword() + " is not match with it's ID !");
                return "Update Failed!! password: "+ kalos.getPassword() + " is not match with it's ID !";
            }
        }

    }

    //使用參數idkalos 刪除使用者
    @DeleteMapping("/delete")
    public String delete(@RequestParam(value = "idkalos",required = false)Integer idkalos ){
            if(idkalos == null){
                return "Need paramID to delete!";
            }else {
                try{
                    kalosRepo.deleteById(idkalos);
                    return "ID :" + idkalos + " is died!";
                }catch (Exception e){
                    return "OOPS! ID :" + idkalos + " is already died or never used!";
                }
            }
    }
}