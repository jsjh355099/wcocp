package wcocp.cp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//Junit有提供測試平台Junit Platform
//預設Junit 5 單元測試方法與腳本編寫
@SpringBootTest//同test版執行主程序掃描從級以下的bean物件並擁有Spring IOC特性(@Autowired)(@Transactional結束後自動回滾)
class CpApplicationTests {


	@Test//	需使用Junit_5:jupiter版本註解
	void contextLoads() {
	}
}
